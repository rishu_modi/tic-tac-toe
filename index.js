/**
 * This program is a boliler plate code for the famous tic tac toe game
 * Here box represents one placeholder for either X or a 0
 * We have a 2D array to represent the arrangement of X or O is a grid
 * 0 -> empty box
 * 1 -> box with X
 * 2 -> box with O
 *
 * Below are the tasks which needs to be completed
 * Imagine you are playing with Computer so every alternate move should be by Computer
 * X -> player
 * O -> Computer
 *
 * Winner has to be decided and has to be flashed
 *
 * Extra points will be given for the Creativity
 *
 * Use of Google is not encouraged
 *
 */
const grid = [];
const GRID_LENGTH = 3;
let turn = 'X';
let counter = 0;

function initializeGrid() {
  for (let colIdx = 0; colIdx < GRID_LENGTH; colIdx++) {
    const tempArray = [];
    for (let rowidx = 0; rowidx < GRID_LENGTH; rowidx++) {
      tempArray.push(0);
    }
    grid.push(tempArray);
  }
}

function getRowBoxes(colIdx) {
  let rowDivs = '';

  for (let rowIdx = 0; rowIdx < GRID_LENGTH; rowIdx++) {
    let additionalClass = 'darkBackground';
    let content = '';
    const sum = colIdx + rowIdx;
    if (sum % 2 === 0) {
      additionalClass = 'lightBackground'
    }
    const gridValue = grid[colIdx][rowIdx];
    if (gridValue === 1) {
      content = '<span class="cross">X</span>';
    } else if (gridValue === 2) {
      content = '<span class="oval">O</span>';
    }
    rowDivs = rowDivs + '<div colIdx="' + colIdx + '" rowIdx="' + rowIdx + '" class="box ' +
      additionalClass + '">' + content + '</div>';
  }
  return rowDivs;
}

function getColumns() {
  let columnDivs = '';
  for (let colIdx = 0; colIdx < GRID_LENGTH; colIdx++) {
    let coldiv = getRowBoxes(colIdx);
    coldiv = '<div class="rowStyle">' + coldiv + '</div>';
    columnDivs = columnDivs + coldiv;
  }
  return columnDivs;
}

function renderMainGrid() {
  const parent = document.getElementById("grid");
  const columnDivs = getColumns();
  parent.innerHTML = '<div class="columnsStyle">' + columnDivs + '</div>';
}

function onBoxClick(el) {
  // el.target.removeEventListener('click',onBoxClick); Tried to remove the event listener from the div
  counter += 1;
  var rowIdx = this.getAttribute("rowIdx");
  var colIdx = this.getAttribute("colIdx");
  if (counter % 2 === 0) {
    let newValue = 1;
    grid[colIdx][rowIdx] = newValue;
    renderMainGrid();
    addClickHandlers();
  } else {
    let newValue = 2;
    grid[colIdx][rowIdx] = newValue;
    renderMainGrid();
    addClickHandlers();
  }

  var finalObject = {
    ifVerticalX: checkVertical(grid, 1, 'X'),
    ifVerticalO: checkVertical(grid, 2, 'O'),
    ifHorizontalX: checkHorizontal(grid, 1, 'X'),
    ifHorizontalO: checkHorizontal(grid, 2, 'O'),
    ifDiagonalLeftO: checkDiagonalLeft(grid, 2),
    ifDiagonalLeftX: checkDiagonalLeft(grid, 1),
    ifDiagonalRightO: checkDiagonaRight(grid, 2),
    ifDiagonalRightX: checkDiagonaRight(grid, 1)
  };
  if (finalObject.ifVerticalX) {
    document.getElementById('winner').innerHTML = 'Player X wins!!!';
    removeClickHandlers();
  } else if (finalObject.ifVerticalO) {
    document.getElementById('winner').innerHTML = 'Player O wins!!!';
    removeClickHandlers();
  } else if (finalObject.ifHorizontalX) {
    document.getElementById('winner').innerHTML = 'Player X wins!!!';
    removeClickHandlers();
  } else if (finalObject.ifHorizontalO) {
    document.getElementById('winner').innerHTML = 'Player O wins!!!';
    removeClickHandlers();
  } else if (finalObject.ifDiagonalLeftO) {
    document.getElementById('winner').innerHTML = 'Player O wins!!!';
    removeClickHandlers();
  } else if (finalObject.ifDiagonalLeftX) {
    document.getElementById('winner').innerHTML = 'Player X wins!!!';
    removeClickHandlers();
  } else if (finalObject.ifDiagonalRightO) {
    document.getElementById('winner').innerHTML = 'Player O wins!!!';
    removeClickHandlers();
  } else if (finalObject.ifDiagonalRightX) {
    document.getElementById('winner').innerHTML = 'Player X wins!!!';
    removeClickHandlers();
  }
  //For checking the match tied case
  // if (counter === 9) {
  //   document.getElementById('winner').innerHTML = 'Match Tied. Play Again!!'
  // }
}

function addClickHandlers() {
  var boxes = document.getElementsByClassName("box");
  for (var idx = 0; idx < boxes.length; idx++) {
    boxes[idx].addEventListener('click', onBoxClick, removeClickHandlers); // To make clickable just once
  }
}

function removeClickHandlers() {
  var boxes = document.getElementsByClassName("box");
  for (var idx = 0; idx < boxes.length; idx++) {
    boxes[idx].removeEventListener('click', onBoxClick);
  }
}

function checkVertical(grid, value) {
  for (i = 0; i < GRID_LENGTH; ++i) {
    if (grid[0][i] === value && grid[1][i] === value && grid[2][i] === value) {
      return true;
    }
  }
  return false;
}

function checkHorizontal(grid, value) {
  for (i = 0; i < GRID_LENGTH; ++i) {
    if (grid[i][0] === value && grid[i][1] === value && grid[i][2] === value) {
      return true;
    }
  }
  return false;
}

function checkDiagonalLeft(grid, value) {
  if (grid[0][0] === value && grid[1][1] === value && grid[2][2] === value) {
    return true;
  }
  return false;
}

function checkDiagonaRight(grid, value) {
  if (grid[0][2] === value && grid[1][1] === value && grid[2][0] === value) {
    return true;
  }
  return false;
}

function reset() {
  document.getElementById('winner').innerHTML = "";
  location.reload(true); // For refreshing the page
}

initializeGrid();
renderMainGrid();
addClickHandlers();
